Steps to Setup

**1. Clone the repository** 

```bash
git clone https://gitlab.com/NandiniSn/task1.git
```


**2. Change the folder location in Fileoperations.java**

```
change FOLDER_PATH according to ur path 
```

That's it! The application can be accessed at `http://localhost:8080`.

Have used POSTMAN API to load the file in POST,GET Method

1) http://localhost:8080/Filecrud/test/upload --> to upload the file 
2) http://localhost:8080/Filecrud/test/download/filename = "test"
3) http://localhost:8080/Filecrud/test/delete/filename = "test"
package com.JavaFileMode.Rest;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.commons.io.FileUtils;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.core.*;
import com.sun.jersey.core.header.FormDataContentDisposition;
import com.sun.jersey.multipart.FormDataParam;
import com.sun.xml.internal.bind.v2.schemagen.xmlschema.List;

 
@Path("test")
public class Fileoperations {
 
    
	private static final String FOLDER_PATH = "C:/temp"; 
    @POST
    @Path("/upload")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.TEXT_PLAIN)
    public String uploadFile(@FormDataParam("file") InputStream fis,
                    @FormDataParam("file") FormDataContentDisposition fdcd) {
  
        OutputStream outpuStream = null;
        String fileName = fdcd.getFileName();
        System.out.println("File Name: " + fdcd.getFileName());
        String filePath = FOLDER_PATH + fileName;
         
        try {
            int read = 0;
            byte[] bytes = new byte[1024];
            outpuStream = new FileOutputStream(new File(filePath));
            while ((read = fis.read(bytes)) != -1) {
                outpuStream.write(bytes, 0, read);
            }
            outpuStream.flush();
            outpuStream.close();
        } catch(IOException iox){
            iox.printStackTrace();
        } finally {
            if(outpuStream != null){
                try{outpuStream.close();} catch(Exception ex){}
            }
        }
        return "File Upload Successfully !!";
    }
    
    @GET
	@Path("/download/file")
	@Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response downloadFilebyPath(@QueryParam("file") String downfile) throws IOException {
        return download(downfile);
      }  
    
    private Response download(String fileName) throws IOException {         	
        String fileLocation = FOLDER_PATH + fileName;
        Response response = null;       
        // Retrieve the file 
        try
        {
        File file = new File(FOLDER_PATH + fileName);
        if (file.exists()) {
          ResponseBuilder builder = Response.ok(file);
          builder.header("Content-Disposition", "attachment; filename=" + file.getName());
          response = builder.build();        
        } 
        else 
        {
         response = Response.status(404).
         entity("FILE NOT FOUND: " + fileLocation).
         type("text/plain").build();
        }
         }
        catch(Exception e){
            e.printStackTrace();
        }
		return response;
      }
    
      
    @DELETE
   	@Path("/delete/file")  
   	@Produces(MediaType.APPLICATION_OCTET_STREAM)
       public void deleteFilebyPath(@QueryParam("file") String delfile) throws IOException {
    	String fileLocation = FOLDER_PATH + delfile;
    	FileUtils.touch(new File(fileLocation));
        File fileToDelete = FileUtils.getFile("src/test/resources/fileToDelete_commonsIo.txt");
        boolean success = FileUtils.deleteQuietly(fileToDelete);
         }  
      
    
}
